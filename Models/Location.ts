import { LocalPlayer, MapArea, Team, Unit, Vector3 } from "../wrapper/Imports";
import { LocationX } from "../X-Core/Imports";

export class LocationFarmX  {

	public BotPath: Vector3[] = []
	public MidPath: Vector3[] = []
	public TopPath: Vector3[] = []
	public LaneCache = new Map<Unit, Vector3[]>()

	constructor() {

		const flag = LocalPlayer!.Team === Team.Radiant
		this.TopPath = (flag ? LocationX.MapLocation.RadiantTopRoute : LocationX.MapLocation.DireTopRoute)
		this.MidPath = (flag ? LocationX.MapLocation.RadiantMiddleRoute : LocationX.MapLocation.DireMiddleRoute)
		this.BotPath = (flag ? LocationX.MapLocation.RadiantBottomRoute : LocationX.MapLocation.DireBottomRoute)
	}

	public IsJungle(pos: Vector3) {
		return LocationX.MapLocation.DireBottomJungle.IsInside(pos)
			|| LocationX.MapLocation.DireTopJungle.IsInside(pos)
			|| LocationX.MapLocation.RadiantBottomJungle.IsInside(pos)
			|| LocationX.MapLocation.RadiantTopJungle.IsInside(pos)
	}

	public GetLane(hero: Unit): MapArea {
		const lane = LocationX.MapLocation.GetLane(hero.Position)
		if (lane <= MapArea.DireBottomJungle) {
			switch (lane) {
				case MapArea.Top:
					return MapArea.Top
				case MapArea.Middle:
					return MapArea.Middle
				case MapArea.Top | MapArea.Middle:
					break
				case MapArea.Bottom:
					return MapArea.Bottom
				default:
					if (lane === MapArea.DireBottomJungle) {
						return MapArea.Bottom
					}
					break
			}
		} else {
			if (lane === MapArea.DireTopJungle)
				return MapArea.Top
			if (lane === MapArea.RadiantBottomJungle)
				return MapArea.Bottom
			if (lane === MapArea.RadiantTopJungle)
				return MapArea.Top
		}
		return MapArea.Middle
	}

	public GetPath(hero: Unit): Vector3[] {
		const lane = this.GetLane(hero)
		if (lane <= MapArea.DireBottomJungle) {
			switch (lane) {
				case MapArea.Top:
					return this.TopPath
				case MapArea.Middle:
					return this.MidPath
				case MapArea.Top | MapArea.Middle:
					break
				case MapArea.Bottom:
					return this.BotPath
				default:
					if (lane === MapArea.DireBottomJungle) {
						return this.BotPath
					}
					break
			}
		} else {
			if (lane === MapArea.DireTopJungle)
				return this.TopPath
			if (lane === MapArea.RadiantBottomJungle)
				return this.BotPath
			if (lane === MapArea.RadiantTopJungle)
				return this.TopPath
		}
		return this.MidPath
	}

	public GetPathCache(hero: Unit): Vector3[] {
		if (!this.LaneCache.has(hero))
			this.LaneCache.set(hero, this.GetPath(hero))
		return this.LaneCache?.get(hero) ?? []
	}
}
