import { Menu as MenuSDK } from "wrapper/Imports"

/*** @AUTO_FARM */
const AutoFarmMenuTree = MenuSDK.AddEntryDeep(["Utility", "Illusion Farm"])
export const State = AutoFarmMenuTree.AddToggle("State", false, "auto control & farm illusion")
export const FarmEnemyCamp = AutoFarmMenuTree.AddToggle("Enemy camps", true)

export const FarmAncientCamps = AutoFarmMenuTree.AddToggle("Ancient camp", false)
export const FarmType = AutoFarmMenuTree.AddDropdown("Mode", ["Jungle", "Lanes", "Auto"], 2)
export const IllusionsOnOneCamp = AutoFarmMenuTree.AddSlider("Illusions on one camp", 2, 1, 6)
export const IllusionsOnOneLane = AutoFarmMenuTree.AddSlider("Illusions on lane", 1, 1, 6)

export let FarmSettings = {
	State,
	FarmType,
	FarmEnemyCamp,
	FarmAncientCamps,
	IllusionsOnOneCamp,
	IllusionsOnOneLane,
}

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Mode", "Режим"],
	["Lanes", "Лайны"],
	["Auto", "Автоматический"],
	["Ancient camp", "Лагерь древних"],
	["Enemy camps", "Вражеские лагеря"],
	["Illusions on lane", "Иллюзий на лайн"],
	["Illusions on one camp", "Иллюзий на один лагерь"],
	["auto control & farm illusion", "авто-контроль и фарм иллюзиями"],
]))
