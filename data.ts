import { Base } from "./Models/Units";
import { AutoFarmUnits } from "./Service/Controller";
import { ParticlesSDK } from "./wrapper/Imports";

export enum FarmTypeEnum {
	Jungle,
	Lane,
	Auto,
	TopLane,
	MiddleLane,
	BotLane,
	Unknown,
}

export class AUTO_FARM_DATA {
	public static UsrBlockOrder = false
	public static BaseUnits: Base[] = []
	public static AutoFarmer: Nullable<AutoFarmUnits>
	public static particleSDK = new ParticlesSDK()

	public static Dispose() {
		this.BaseUnits = []
		this.UsrBlockOrder = false
		this.AutoFarmer?.Dispose()
		this.particleSDK.DestroyAll(true)
	}
}
